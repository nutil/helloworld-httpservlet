# Java Servlet HelloWorld war file example

java servlet Hello World minimum example .war file

* Requires: javax.servlet-api
```
curl -L -O https://repo1.maven.org/maven2/javax/servlet/javax.servlet-api/4.0.1/javax.servlet-api-4.0.1.jar
```

## compile
```
javac -classpath ./javax.servlet-api-4.0.1.jar HelloWorld.java
```

## pre package
```
cp HelloWorld.class WEB-INF/classes/
```

## package war
```
jar cvf servlet.war META-INF WEB-INF
```

## deploy .war file (tomcat example)
```
cp servlet.war $CATALINA_BASE/webapps/servlet.war
```

## test url
```
localhost:8080/servlet/HelloWorld
```


